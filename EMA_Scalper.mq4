//+------------------------------------------------------------------+
//|                                                  EMA_Scalper.mq4 |
//|                                                         Henku Oy |
//+------------------------------------------------------------------+
#property strict
#property copyright "Henku Oy"
#property link      ""
#property version   "0.99"
string    version = "0.99";


input const double LOT_SIZE = 0.01;
input const double MAX_RISK_AMOUNT = 1.3;
input const double RISK_REWARD_RATIO = 2.0;
input const int MAX_SPREAD = 10;
input const int MAX_SLIPPAGE = 5;

// input const int STOP_LOSS_PIPS = 0;
const int STOP_LOSS_PIPS = 0;
// input const bool Mail_Alert = false;
// input const bool PopUp_Alert = false;
// input const bool Sound_Alert = false;
// input const bool SmartPhone_Notifications = false;
input const int MAGIC_NUMBER = 429457;

const double PROFIT_TARGET_AMOUNT = MAX_RISK_AMOUNT * RISK_REWARD_RATIO;
bool BUYS_ALLOWED;
bool SELLS_ALLOWED;
double UsePoint;
int UseSlippage;

double ema50;
double ema100;
double ema200;
bool thisBarHasTradeOpen;
bool printedSpreadMessageForThisBar;


// Hooks
int OnInit(){
   UsePoint = getDecimalPip();
   UseSlippage = getSlippage();
   BUYS_ALLOWED = true;
   SELLS_ALLOWED = true;
   thisBarHasTradeOpen = false;
   printedSpreadMessageForThisBar = false;

   return(INIT_SUCCEEDED);
}

void OnDeinit(const int reason) {
   // ChartBackColorSet(Black, 0);
}

void OnTick() {
   RefreshRates();
   closeTrades();
   displayInfo();

   if(isNewBar()) {
      thisBarHasTradeOpen = false;
      printedSpreadMessageForThisBar = false;
   }
   
   if(thisBarHasTradeOpen) {
      Sleep(10000);
      return;
   }
   
   RefreshRates();
   updateEMAs();

   if(isBuyTrend() && isBuySignal()) {
      placeTrade(OP_BUY);
   } else if(isSellTrend() && isSellSignal()) {
      placeTrade(OP_SELL);
   }
   
   Sleep(200);
}


// Methods
bool isBuySignal() {
   bool previousBarBrokeEMA = Low[1] < ema50 && Close[1] > ema50;
   bool currentPriceBrokeLastBar = High[1] < Ask;
   
   return previousBarBrokeEMA && currentPriceBrokeLastBar;
}

bool isSellSignal() {
   bool previousBarBrokeEMA = High[1] > ema50 && Close[1] < ema50;
   bool currentPriceBrokeLastBar = Low[1] > Bid;
   
   return previousBarBrokeEMA && currentPriceBrokeLastBar;
}

bool isBuyTrend() {
   return ema50 > ema100 && ema100 > ema200;
}

bool isSellTrend() {
   return ema50 < ema100 && ema100 < ema200;
}

void closeTrades() {
   for(int i = 0; i < OrdersTotal(); i++) {
      if(!OrderSelect(i, SELECT_BY_POS, MODE_TRADES)) {
         Print("Failed to select order for closing: " + (string)GetLastError());
         return;
      }

      if(OrderSymbol() == ChartSymbol() && OrderMagicNumber() == MAGIC_NUMBER) {
         RefreshRates();

         double orderProfit = OrderProfit();
         double tradeCost = OrderSwap() + OrderCommission();
         bool tradeInProfit = orderProfit > PROFIT_TARGET_AMOUNT;
         bool tradeTotalInProfit = (orderProfit + tradeCost) >= PROFIT_TARGET_AMOUNT;
         bool tradeAtStopLoss = orderProfit < 0 && MAX_RISK_AMOUNT + tradeCost + orderProfit <= 0;

         if(tradeTotalInProfit && tradeInProfit) {
            Print("Closing order with PROFIT(" + (string)orderProfit + "+" + (string)OrderSwap() + "+" + (string)OrderCommission() + ")");
            if(!OrderClose(OrderTicket(), OrderLots(), Bid, UseSlippage)) {
               if(GetLastError() == 136)  {
                  continue;
               } else {
                  Print("Failed to close trade: " + (string)GetLastError());
               }
            }
         } else if(tradeAtStopLoss) {
            Print("Closing order with LOSS(" + (string)orderProfit + "+" + (string)OrderSwap() + "+" + (string)OrderCommission() + ")");
            if(!OrderClose(OrderTicket(), OrderLots(), Bid, UseSlippage)) {
               if(GetLastError() == 136)  {
                  continue;
               } else {
                  Print("Failed to close trade: " + (string)GetLastError());
               }
            }
         }
      }
   }
}

void updateEMAs() {
   ema50 = iMA(
      Symbol(), // Current symbol
      PERIOD_CURRENT, // Current timeframe
      50, // 50 Period
      0, // No offset
      MODE_EMA, // Exponential Moving Average
      PRICE_CLOSE, // Closing Price
      1 // Shift
   );
   
   ema100 = iMA(
      Symbol(), // Current symbol
      PERIOD_CURRENT, // Current timeframe
      100, // 100 Period
      0, // No offset
      MODE_EMA, // Exponential Moving Average
      PRICE_CLOSE, // Closing Price
      1 // Shift
   );
   
   ema200 = iMA(
      Symbol(), // Current symbol
      PERIOD_CURRENT, // Current timeframe
      200, // 200 Period
      0, // No offset
      MODE_EMA, // Exponential Moving Average
      PRICE_CLOSE, // Closing Price
      1 // Shift
   );
}




void displayInfo() {
   Comment("EMA Scalper " + version + "\n");
}

bool placeTrade(int type) {
   RefreshRates();

   int lotDigits = (int) - MathLog10(SymbolInfoDouble(Symbol(), SYMBOL_VOLUME_STEP));
   double lotSize = NormalizeDouble(LOT_SIZE, lotDigits);

   double spread = Ask - Bid;
   if(spread > MAX_SPREAD * UsePoint) {
      if(!printedSpreadMessageForThisBar) {
         Print("Spread too large, can't place trade: " + (string)spread);
         printedSpreadMessageForThisBar = true;
      }
      
      return(false);
   }

   int ticket = -1;
   if(type == OP_BUY) {
      double takeProfit = 0; // TakeProfitPips == 0 ? 0 : NormalizeDouble(Bid + spread + TakeProfitPips * UsePoint, Digits);
      double stopLoss = STOP_LOSS_PIPS == 0 ? 0 : NormalizeDouble(Bid + spread - STOP_LOSS_PIPS * UsePoint, Digits);
      ticket = OrderSend(Symbol(), OP_BUY, lotSize, Ask, MAX_SLIPPAGE, stopLoss, takeProfit, "Spread Test", MAGIC_NUMBER, 0, Green);
   } else if(type == OP_SELL) {
      double takeProfit = 0; // TakeProfitPips == 0 ? 0 : NormalizeDouble(Bid - spread - TakeProfitPips * UsePoint, Digits);
      double stopLoss = STOP_LOSS_PIPS == 0 ? 0 : NormalizeDouble(Bid - spread + STOP_LOSS_PIPS * UsePoint, Digits);
      ticket = OrderSend(Symbol(), OP_SELL, lotSize, Bid, MAX_SLIPPAGE, stopLoss, takeProfit, "Spread Test", MAGIC_NUMBER, 0, Green);
   }
   
   Print("Trade Open - 50 EMA: " + (string)ema50 + " 100 EMA: " + (string)ema100 + " 200 EMA: " +
         (string)ema200 + " High[1]: " + (string)High[1] + " Low[1]: " + (string)Low[1] +
         " Ask: " + (string)(type == OP_BUY ? Ask : Bid));
   
   if(GetLastError() == 4110) {
      Print("Buys are not allowed. Buys will not be attempted from now on.");
      BUYS_ALLOWED = false;
      return false;
   } else if(GetLastError() == 4111) {
      Print("Sells are not allowed. Sells will not be attempted from now on.");
      SELLS_ALLOWED = false;
      return false;
   }

   int lastError = GetLastError();
   if(ticket < 0 && lastError > 0) {
      // sendAlert("Error(" + (string)ticket + "): " + (string)lastError);
      return(false);
   }
   
   thisBarHasTradeOpen = true;

// sendAlert("New trade opened: " + (string)type + " - " + Symbol() + " - " + (string)Bid);
   return(true);
}


// Common methods
double getDecimalPip() {
   switch(Digits) {
      case 5:
         return(0.0001);
      case 4:
         return(0.0001);
      case 3:
         return(0.001);
      default:
         return(0.01);
   }
}

bool isNewBar() {
   static datetime NewCandleTime;

   // Ensure correct result when starting robot
   if(NewCandleTime == NULL) {
      NewCandleTime = iTime(Symbol(),0,0);
      return false;
   }

   if(NewCandleTime == iTime(Symbol(),0,0)) {
      return false;
   } else {
      NewCandleTime = iTime(Symbol(),0,0);
      return true;
   }
}

int getSlippage() {
   if(Digits() == 2 || Digits() == 4) return(MAX_SLIPPAGE);
   else if(Digits() == 3 || Digits() == 5) return(MAX_SLIPPAGE * 10);
   return(Digits());
}

// bool ChartBackColorSet(const color clr,const long chart_ID = 0) {
//    ResetLastError();
// 
//    if(!ChartSetInteger(chart_ID, CHART_COLOR_BACKGROUND, clr)) {
// 
//       Print(__FUNCTION__ + ", Error Code = ", GetLastError());
//       return(false);
//    }
// 
//    return(true);
// }

// void sendAlert(string message) {
//    Print(message);
//    if(Mail_Alert) SendMail("New Finch Alert", message);
//    if(PopUp_Alert) Alert(message);
//    if(Sound_Alert) PlaySound("alert.wav");
//    if(SmartPhone_Notifications) SendNotification(message);
//    return;
// }

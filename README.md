# EMA Scalper

A scalping Expert Advisor for MT4.

## Strategy
We follow simple short-term trends and buy the dips for a 1:2 risk reward-ratio. The intended time-frame is 15min to enable intra-day scalping with enough room for volatility.

### Buy Signal
```
1. The Exponential Moving Averages are in order from highest to lowest: 50EMA, 100EMA, 200EMA
2. The previous candle has crossed the 50EMA line and closed above it
3. Buy triggers when Ask price breaks the high of the previous candle
```

### Sell Signal
```
1. The Exponential Moving Averages are in order from lowest to highest: 50EMA, 100EMA, 200EMA
2. The previous candle has crossed the 50EMA line and closed below it
3. Sell triggers when Bid price breaks the low of the previous candle
```

## Inputs, Defaults & Recommender Settings

**LOT_SIZE**: Lot-size to be used (0.01, eg a micro-lot)

**MAX_RISK_AMOUNT**: The amount in the account currency that triggers a Stop-Loss. Swap and comission fees are taken into account, eg positive swap gives the trade additional space, and negative swap reduces space. (1.3);

**RISK_REWARD_RATIO**: The risk-to-reward ratio of the trade, which acts as a multiplier for **MAX_RISK_AMOUNT**. **MAX_RISK_AMOUNT** * **RISK_REWARD_RATIO** = Take-Profit in dollars (2.0, eg risk-to-reward of 1:2);

**MAX_SPREAD**: The maximum spread allowed for a trade. If the spread is above this, a trade will not be triggered (10 points);

**MAX_SLIPPAGE**: The maximum slippage allowed for a trade. If the slippage is above this, a trade will not be triggered (5 points);

The defaults reflect the recommender settings. The **MAX_RISK_AMOUNT** value is recommended to be 1.3 for each micro-lot when working on a 15 minute time-frame.;
